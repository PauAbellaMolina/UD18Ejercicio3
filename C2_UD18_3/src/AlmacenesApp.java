import java.sql.Connection;
import methods.*;
import dto.*;

public class AlmacenesApp {
	
	public static Connection conexion;

	public static void main(String[] args) {
		
		openConnection.openConnection();
		
		DatabaseDto newDatabase = new DatabaseDto("Almacenes");
		createDB.createDB(newDatabase);
		
		TableDto newTable1 = new TableDto(newDatabase, "Almacenes");
		String newValue1 = "CREATE TABLE `Almacenes` (`Codigo` int NOT NULL AUTO_INCREMENT, `Lugar` varchar(100) DEFAULT NULL, `Capacidad` int DEFAULT NULL, PRIMARY KEY (`Codigo`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci";
		createTable.createTable(newDatabase, newTable1, newValue1);
		insertData.insertData(newDatabase, "Almacenes", "(Codigo, Lugar, Capacidad) VALUE(1, 'Chicago', 3);");
		insertData.insertData(newDatabase, "Almacenes", "(Codigo, Lugar, Capacidad) VALUE(2, 'Chicago', 4);");
		insertData.insertData(newDatabase, "Almacenes", "(Codigo, Lugar, Capacidad) VALUE(3, 'New York', 7);");
		insertData.insertData(newDatabase, "Almacenes", "(Codigo, Lugar, Capacidad) VALUE(4, 'Los Angeles', 2);");
		insertData.insertData(newDatabase, "Almacenes", "(Codigo, Lugar, Capacidad) VALUE(5, 'San Francisco', 8);");
		
		TableDto newTable2 = new TableDto(newDatabase, "Cajas");
		String newValue2 = "CREATE TABLE `Cajas` (`NumReferencia` char(5) NOT NULL, `Contenido` varchar(100) DEFAULT NULL, `Valor` int DEFAULT NULL, `Almacen` int DEFAULT NULL, PRIMARY KEY (`NumReferencia`), KEY `Almacen_idx` (`Almacen`), CONSTRAINT `Almacen` FOREIGN KEY (`Almacen`) REFERENCES `Almacenes` (`Codigo`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci";
		createTable.createTable(newDatabase, newTable2, newValue2);
		insertData.insertData(newDatabase, "Cajas", "(NumReferencia, Contenido, Valor, Almacen) VALUE('0MN7', 'Rocks', 153, 3);");
		insertData.insertData(newDatabase, "Cajas", "(NumReferencia, Contenido, Valor, Almacen) VALUE('4H8P', 'Rock', 212.5, 1);");
		insertData.insertData(newDatabase, "Cajas", "(NumReferencia, Contenido, Valor, Almacen) VALUE('4RT3', 'Scissors', 161.5, 4);");
		insertData.insertData(newDatabase, "Cajas", "(NumReferencia, Contenido, Valor, Almacen) VALUE('7G3H', 'Rocks', 170, 1);");
		insertData.insertData(newDatabase, "Cajas", "(NumReferencia, Contenido, Valor, Almacen) VALUE('9J6F', 'Papers', 148.75, 2);");
		
		closeConnection.closeConnection();

	}

}
