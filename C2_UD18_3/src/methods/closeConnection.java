package methods;

import java.sql.SQLException;

public class closeConnection {
	
	public static void closeConnection() {
		
		try {
			
			openConnection.conexion.close();
			System.out.println("Server Disconnected");
			
		} catch (SQLException ex) {
			
			System.out.println(ex.getMessage());
			System.out.println("Error cerrando conexion");
			
		}
		
	}
	
}
