package methods;

import java.sql.SQLException;
import java.sql.Statement;

import dto.DatabaseDto;

public class createDB {

	public static void createDB(DatabaseDto nombre) {
		
		try {
			
			String Query = "CREATE DATABASE " + nombre.getNombre();
			Statement st = openConnection.conexion.createStatement();
			st.executeUpdate(Query);
			
		} catch (SQLException ex) {
			
			System.out.println("Error creando base de datos");
			
		}
		
	}
	
}
