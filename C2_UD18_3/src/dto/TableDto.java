package dto;

public class TableDto {

	private String nombreDB = "";
	private String nombreTabla = "";
	
	public TableDto(DatabaseDto nombre, String nombreTabla) {
		
		this.nombreDB = DatabaseDto.nombre;
		this.nombreTabla = nombreTabla;
		
	}
	
	public String getNombreDB() {
		
		return nombreDB;
		
	}
	
	public void setNombre(String nombreDB) {
		
		this.nombreDB = nombreDB;
		
	}
	
	public String getNombreTabla() {
		
		return nombreTabla;
		
	}
	
	public void setNombreTabla(String nombreTabla) {
		
		this.nombreTabla = nombreTabla;
		
	}
	
}
